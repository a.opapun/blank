#clear environment
rm(list=ls())

#load workspace if there is one
if (file.exists("D:/data/mobile/data.RData")==TRUE){
  load("D:/data/mobile/data.RData")
}

#import data from files (import from database in v2)
#declare function to clean files in order to import
#changing date function
todate <- function(x) {
  y<-as.Date(x, format = "%d/%m/%Y",origin = "1899-12-30")
  #<-dmy(x)
  y1<-year(y)>=2600
  y2<-year(y)>2500&year(y)<2600
  for (i in 1:length(y)){
    if (is.na(y1[i])==TRUE | is.na(y2[i])==TRUE ){
      next
    }
    if (y1[i]==TRUE){
      y[i]<-y[i]-years(586)
    }
    if(y2[i]==TRUE){ 
      y[i]<-y[i]-years(543)
    }
  }
  return(y)
}

#changing file names function
#all_rename(path) -> rename file in the path from mm-bbbb-sp.xlsx to bbbb-mm-sp.xlsx
#rename only the specific name format
all_rename<-function(a){
  files<-list.files(path = a,pattern = "*.xlsx")
  ifile<-length(files)
  for (i in seq_along(files)){
    if (unlist(strsplit(files[i],"[[:punct:]]"))[1]>=2563){
      ifile<-i-1
      break
    }
  }
  if (ifile==0){
    return("no rename made")
  }else{
    for (i in 1:ifile){
      if (is.na(my(files[i]))==F){
        file.rename(files[i], 
                    paste(format.Date(my(files[i]),"%Y"),"-",
                          format.Date(my(files[i]),"%m"),"-",
                          unlist(strsplit(files[i],"[[:punct:]]"))[3],".",
                          unlist(strsplit(files[i],"[[:punct:]]"))[4],sep = ""))}
      else{
        file.rename(files[i], 
                    paste(format.Date(ym(files[i]),"%Y"),"-",
                          format.Date(ym(files[i]),"%m"),"-",
                          unlist(strsplit(files[i],"[[:punct:]]"))[3],".",
                          unlist(strsplit(files[i],"[[:punct:]]"))[4],sep = ""))
      }
    }
    return(cat(ifile," files were renamed"))
  }
}
#add data function -> add data from list of files to data
h1<-read.csv("D:/data/mobile/.header.csv", sep="|")
h1<-h1[,1:76]
add_data<- function(files,mdata){
  isp<-NULL
  for (i in seq_along(files)){
    srcs<-read_xlsx(files[i],col_names = FALSE,skip=8)
    #clean file
    colnames(srcs)<-colnames(h1)
    unlist(strsplit(files[i],"[[:punct:]]"))[3]->isp
    srcs<-mutate(srcs,"month"=ym(files[i])-years(543)+months(1)-days(1),"isp"=isp)
    srcs%>%relocate("isp",.before="code")%>% relocate("month",.before="isp")-> cdata
    cdata[,9:12]<-lapply(cdata[,9:12], FUN = todate)
    cdata%>%mutate(across(everything(),as.character))->cdata
    mdata <- bind_rows(mdata, cdata)
  }
  return(mdata)
}

#function to modify only the file we want
nfile<-function(files,year,month){
  for (i in seq_along(files)){
    if(ym(files[i])==ym(paste(year,"-",month))){
      x=i-1
      break
    }
  }
  return(files[-1:-x])
}


#create database
#setwd
setwd("D:/data/mobile")

#modify files
all_rename("D:/data/mobile")
h1<-read.csv("D:/data/mobile/.header.csv", sep="|")
h1<-h1[,1:76]

#if mdata exist in this workspace then skip else create mdata
if (exists("mdata")==FALSE){
  mdata<-NULL
  files <- list.files(path = "D:/data/mobile", pattern = "*.xlsx")
  mdata<-add_data(files, mdata)
}else{
  #if mdata exists then create array of new files
  #find latest info of mdata of each isp
  nfiles <- list.files("D:/data/mobile",pattern = "*.xlsx")
  nfiles <- nfiles[!is.element(nfiles,files)]
  if(length(nfiles)>0){
    mdata<-add_data(nfiles, mdata)
    files <- list.files(path = "D:/data/mobile",pattern = "*.xlsx")
  }
}


#clean and retype mdata
mdata<-arrange(mdata, mdata$month)
mdata$subtype<-tolower(mdata$subtype)
mdata$type<-tolower(mdata$type)
mdata$status<-tolower(mdata$status)
mdata$isp<-tolower(mdata$isp)
mdata<-retype(mdata)


#save workspace
save.image("D:/data/mobile/data.RData")

###test git

###edit 1

